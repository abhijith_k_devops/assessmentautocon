package com.my.assessmentapp.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.my.assessmentapp.data.local.db.dao.HistoryDao
import com.my.assessmentapp.data.local.db.dao.HistoryRemoteKeyDao
import com.my.assessmentapp.data.local.model.History
import com.my.assessmentapp.data.local.model.HistoryRemoteKey


@Database(
    entities = [History::class,HistoryRemoteKey::class],
    version = DatabaseMigrations.DB_VERSION
)
abstract class AssessmentDB : RoomDatabase() {

    companion object {
        private const val DB_NAME = "assessment_db"

        @Volatile
        private var INSTANCE: AssessmentDB? = null

        fun getInstance(context: Context): AssessmentDB =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(context.applicationContext, AssessmentDB::class.java, DB_NAME)
                .addMigrations(*DatabaseMigrations.MIGRATIONS)
                .build()
    }

    abstract fun historyDao(): HistoryDao
    abstract fun remoteKeyDao() : HistoryRemoteKeyDao

}