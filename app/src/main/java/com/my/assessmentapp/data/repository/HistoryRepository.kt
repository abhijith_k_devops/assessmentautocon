package com.my.assessmentapp.data.repository

import androidx.paging.PagingData
import com.my.assessmentapp.data.local.model.History
import kotlinx.coroutines.flow.Flow

interface HistoryRepository {
    suspend fun getHistory(
        status: String,
        pageSize: Int
    ): Flow<PagingData<History>>
}