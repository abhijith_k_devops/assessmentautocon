package com.my.assessmentapp.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = History.TABLE_NAME)
data class History(
    @PrimaryKey
    @ColumnInfo(collate = ColumnInfo.NOCASE)
    val INVH_POYNT_ID: String,
    val INVH_SYS_ID: Double?,
    val HIBERNATE_SORT_ROW: Double?,
    val INVH_CANCEL_YN: String?,
    val INVH_CUR_CODE: String?,
    val INVH_DELV_CHANNEL: String?,
    val INVH_DELV_CHANNEL_NAME: String?,
    val INVH_DISP_NO: Double?,
    val INVH_DT: String?,
    val INVH_DT_KEY: String?,
    val INVH_NET_TOTAL: Double?,
    val INVH_NO: Double?,
    val INVH_PAY_TYPE: String?,
    val INVH_POYNT_STATUS: String?,
    val INVH_RETN_COUNT: Double?,
    val INVH_SERV_MODE: String?,
    val INVH_SM_CODE: String?,
    val INVH_SM_NAME: String?,
    val INVH_TABLE_CODE: String?,
    val INVH_TABLE_NAME: String?,
    val INVH_TM: String?
){
    var indexInResponse: Int = -1

    companion object {
        const val TABLE_NAME = "history"
    }
}
