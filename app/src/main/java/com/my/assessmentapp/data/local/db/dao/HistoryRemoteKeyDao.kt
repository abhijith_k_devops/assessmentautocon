package com.my.assessmentapp.data.local.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.my.assessmentapp.data.local.model.HistoryRemoteKey

@Dao
interface HistoryRemoteKeyDao : BaseDao<HistoryRemoteKey> {

    @Query("SELECT * FROM ${HistoryRemoteKey.TABLE_NAME}")
    suspend fun remoteKey(): HistoryRemoteKey?

    @Query("DELETE FROM ${HistoryRemoteKey.TABLE_NAME}")
    suspend fun clearRemoteKeys()

}