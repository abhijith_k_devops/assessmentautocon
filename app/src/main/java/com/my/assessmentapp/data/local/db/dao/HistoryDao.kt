package com.my.assessmentapp.data.local.db.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Query
import com.my.assessmentapp.data.local.model.History

@Dao
interface HistoryDao : BaseDao<History> {
    @Query("SELECT * FROM ${History.TABLE_NAME}  ORDER BY indexInResponse ASC")
    fun getHistory(): PagingSource<Int, History>
}