package com.my.assessmentapp.data.repository

import android.content.Context
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.my.assessmentapp.data.local.db.AssessmentDB
import com.my.assessmentapp.data.local.model.History
import kotlinx.coroutines.flow.Flow

class HistoryRepositoryImpl(val context: Context, val assessmentDB: AssessmentDB) : HistoryRepository {
    @ExperimentalPagingApi
    override suspend fun getHistory(status: String, pageSize: Int): Flow<PagingData<History>> {
        return Pager(
            config = PagingConfig(pageSize = 10),
            remoteMediator = PageKeyedHistoryRemoteMediator(assessmentDB,context)
        ){
            assessmentDB.historyDao().getHistory()
        }.flow
    }
}