package com.my.assessmentapp.data.repository

import android.content.Context
import androidx.paging.*
import androidx.room.withTransaction

import com.my.assessmentapp.data.local.db.AssessmentDB
import com.my.assessmentapp.data.local.db.dao.HistoryDao
import com.my.assessmentapp.data.local.db.dao.HistoryRemoteKeyDao
import com.my.assessmentapp.data.local.model.History
import com.my.assessmentapp.data.local.model.HistoryRemoteKey
import com.my.assessmentapp.utils.FetchDetails
import java.net.UnknownHostException

@OptIn(ExperimentalPagingApi::class)
class PageKeyedHistoryRemoteMediator(
    private val db: AssessmentDB,
    private val context: Context
) : RemoteMediator<Int,History>() {
    private val remoteKeyDao: HistoryRemoteKeyDao = db.remoteKeyDao()
    private val historyDao : HistoryDao = db.historyDao()
    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, History>
    ): MediatorResult {

        try {
            // Get the closest item from PagingState that we want to load data around.
            val loadKey = when (loadType) {
                LoadType.REFRESH -> 1
                LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> {
                    val remoteKey = db.withTransaction {
                        remoteKeyDao.remoteKey()
                    }
                    if (remoteKey?.nextPageKey == null) {
                        return MediatorResult.Success(endOfPaginationReached = true)
                    }
                    remoteKey.nextPageKey
                }
            }


            val data = FetchDetails(context,loadKey).getJsonDataFromAsset()

            val items = mutableListOf<History>()

            items.addAll(data!!.ResponseData as ArrayList<History>)


            db.withTransaction {
                if (loadType == LoadType.REFRESH) {

                    remoteKeyDao.clearRemoteKeys()
                }
                remoteKeyDao.insert(
                    HistoryRemoteKey(
                        "history",
                        if (data.pageNum!! < 10) data.pageNum + 1 else 1
                    )
                )
                historyDao.insertAll(items)
            }
            return MediatorResult.Success(endOfPaginationReached = items.isEmpty())
        } catch (e: Exception) {
            if (loadType == LoadType.REFRESH) {
                db.withTransaction {
                    remoteKeyDao.clearRemoteKeys()
                }
            }
            return when (e) {
                is UnknownHostException -> {
                    MediatorResult.Error(e)
                }
                else -> {
                    MediatorResult.Error(e)
                }
            }
        }
    }
    /*override suspend fun load(params: LoadParams<Int>): LoadResult<Int, History> {
        try {
            val nextPageNumber = params.key ?: 1
            return LoadResult.Page(
                data = FetchDetails(context,nextPageNumber).getJsonDataFromAsset(),
                prevKey = null, // Only paging forward.
                nextKey = nextPageNumber.plus(1)
            )
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, History>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }*/

}