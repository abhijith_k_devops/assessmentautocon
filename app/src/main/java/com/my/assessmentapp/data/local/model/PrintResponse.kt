package com.my.assessmentapp.data.local.model

data class PrintResponse(
    val ErrCode: String?,
    val ErrMsg: String?,
    val IsSuccess: Boolean?,
    val ResponseData: List<Print?>?
) {
    data class Print(
        val PRINT_BODY: List<PrintBody?>?,
        val PRINT_FOOTER: List<PrintBody?>?,
        val PRINT_HEADER: List<PrintBody?>?
    )
}