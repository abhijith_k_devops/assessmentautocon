package com.my.assessmentapp.data.local.model

data class PrintBody(
    val printAlign1: Int?,
    val printAlign2: Int?,
    val printData1: String?,
    val printData2: String?,
    val printFont1: Int?,
    val printFont2: Int?,
    val printType1: String?,
    val printType2: String?
)
