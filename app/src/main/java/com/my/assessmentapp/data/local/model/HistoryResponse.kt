package com.my.assessmentapp.data.local.model

data class HistoryResponse(
    val ErrCode: String?,
    val ErrMsg: String?,
    val IsSuccess: Boolean?,
    val pageNum: Int?,
    val pageSize: Int?,
    val MID: Any?,
    val OtherData1: Any?,
    val OtherData2: Any?,
    val ReceetData: Any?,
    val ResponseData: List<History?>?,
    val TID: Any?
)