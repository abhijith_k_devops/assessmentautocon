package com.my.assessmentapp.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = HistoryRemoteKey.TABLE_NAME)
data class HistoryRemoteKey(
    @PrimaryKey
    @ColumnInfo(collate = ColumnInfo.NOCASE)
    val status: String,
    val nextPageKey: Int?
) {
    companion object {
        const val TABLE_NAME = "remote_keys"
    }
}