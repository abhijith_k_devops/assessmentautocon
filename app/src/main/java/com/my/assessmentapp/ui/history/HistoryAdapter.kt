package com.my.assessmentapp.ui.history

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.my.assessmentapp.R
import com.my.assessmentapp.data.local.model.History
import com.my.assessmentapp.databinding.LayoutHistoryItemBinding
import com.my.assessmentapp.ui.common.DataBoundPagingDataAdapter

class HistoryAdapter(): DataBoundPagingDataAdapter<History,LayoutHistoryItemBinding>(
    diffCallback = object : DiffUtil.ItemCallback<History>(){
        override fun areItemsTheSame(oldItem: History, newItem: History): Boolean {
            return oldItem.INVH_POYNT_ID == newItem.INVH_POYNT_ID
        }

        override fun areContentsTheSame(oldItem: History, newItem: History): Boolean {
            return oldItem.INVH_POYNT_ID == newItem.INVH_POYNT_ID
        }

    }
) {
    override fun createBinding(parent: ViewGroup): LayoutHistoryItemBinding {
        val binding = DataBindingUtil
            .inflate<LayoutHistoryItemBinding>(
                LayoutInflater.from(parent.context),
                R.layout.layout_history_item,
                parent,
                false
            )
        return binding
    }

    override fun bind(binding: LayoutHistoryItemBinding, item: History) {
        binding.history = item
        binding.code = "Code : ${item.INVH_NO!!.toInt()}"
    }

}