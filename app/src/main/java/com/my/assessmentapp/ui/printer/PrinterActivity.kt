package com.my.assessmentapp.ui.printer

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.my.assessmentapp.AppExecutors
import com.my.assessmentapp.R
import com.my.assessmentapp.data.local.model.PrintBody
import com.my.assessmentapp.databinding.ActivityPrinterBinding
import com.my.assessmentapp.ui.common.VerticalSpacingDecoration
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class PrinterActivity : AppCompatActivity() {

    private val viewModel: PrinterViewModel by viewModel()
    lateinit var binding: ActivityPrinterBinding
    private val appExecutors: AppExecutors by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPrinterBinding.inflate(layoutInflater)
        binding.printList.addItemDecoration(
            VerticalSpacingDecoration(
                this,
                R.dimen.dimen_4
            )
        )
        binding.closeButton.setOnClickListener {
            finish()
        }
        val adapter = PrintAdapter(appExecutors)
        viewModel.printResponse.observe(this, Observer {
            val header = it.ResponseData?.get(0)?.PRINT_HEADER as ArrayList<PrintBody>
            val body = it.ResponseData.get(0)?.PRINT_BODY as ArrayList<PrintBody>
            val footer = it.ResponseData.get(0)?.PRINT_FOOTER as ArrayList<PrintBody>
            val final = ArrayList<PrintBody>()
            final.addAll(header)
            final.addAll(body)
            final.addAll(footer)
            adapter.submitList(final)
        })
        binding.printList.adapter = adapter
        setContentView(binding.root)

    }

}