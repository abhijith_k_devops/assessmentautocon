package com.my.assessmentapp.ui.history

import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import com.my.assessmentapp.R
import com.my.assessmentapp.databinding.ActivityHistoryBinding
import com.my.assessmentapp.ui.common.HistoryLoadStateAdapter
import com.my.assessmentapp.ui.common.VerticalSpacingDecoration
import kotlinx.coroutines.flow.collectLatest
import org.koin.androidx.viewmodel.ext.android.viewModel

class HistoryActivity : AppCompatActivity() {

    val viewModel: HistoryViewModel by viewModel()
    lateinit var binding: ActivityHistoryBinding
    private lateinit var adapter: HistoryAdapter
    @ExperimentalPagingApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHistoryBinding.inflate(layoutInflater)
        initAdapter()
        binding.closeButton.setOnClickListener {
            finish()
        }

        lifecycleScope.launchWhenCreated {
            adapter.loadStateFlow.collectLatest { loadStates ->
                if (::binding.isInitialized) {

                    when (loadStates.refresh) {
                        is LoadState.Error -> {
                            binding.progressBar.visibility = View.GONE
                        }
                        is LoadState.Loading -> {
                            binding.progressBar.visibility = View.VISIBLE
                        }
                        is LoadState.NotLoading -> {
                            binding.progressBar.visibility = View.GONE
                        }
                    }

                }
            }
        }
        setContentView(binding.root)
    }

    @ExperimentalPagingApi
    private fun initAdapter() {

        binding.historyList.addItemDecoration(
            VerticalSpacingDecoration(
                this,
                R.dimen.dimen_4
            )
        )

        adapter = HistoryAdapter()

        binding.historyList.addItemDecoration(
            VerticalSpacingDecoration(
                this,
                R.dimen.dimen_4
            )
        )

        binding.historyList.adapter = adapter.withLoadStateFooter(
            footer = HistoryLoadStateAdapter(adapter)
        )

        lifecycleScope.launchWhenCreated {
            viewModel.history.collectLatest {
                if (::adapter.isInitialized)
                    adapter.submitData(it)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        viewModel.initHistoryListing()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}