package com.my.assessmentapp.ui.printer

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.my.assessmentapp.AppExecutors
import com.my.assessmentapp.R
import com.my.assessmentapp.data.local.model.PrintBody
import com.my.assessmentapp.databinding.LayoutPrintItemBinding
import com.my.assessmentapp.ui.common.DataBoundListAdapter

class PrintAdapter(
    appExecutors: AppExecutors
): DataBoundListAdapter<PrintBody,LayoutPrintItemBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<PrintBody>(){
        override fun areItemsTheSame(oldItem: PrintBody, newItem: PrintBody): Boolean {
            return false
        }

        override fun areContentsTheSame(oldItem: PrintBody, newItem: PrintBody): Boolean {
            return false
        }

    }
)  {
    override fun createBinding(parent: ViewGroup): LayoutPrintItemBinding {
        return DataBindingUtil
            .inflate(
                LayoutInflater.from(parent.context),
                R.layout.layout_print_item,
                parent,
                false
            )
    }

    override fun bind(binding: LayoutPrintItemBinding, item: PrintBody) {
        binding.print = item
    }
}