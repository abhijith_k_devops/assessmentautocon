package com.my.assessmentapp.ui.common

import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import timber.log.Timber
import java.lang.Exception

/**
 * A generic PagingDataAdapter that uses Data Binding & DiffUtil.
 *
 * @param <T> Type of the items in the list
 * @param <V> The type of the ViewDataBinding
</V></T> */
abstract class DataBoundPagingDataAdapter<T : Any, V : ViewDataBinding>(
    diffCallback: DiffUtil.ItemCallback<T>
) : PagingDataAdapter<T, DataBoundViewHolder<V>>(diffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataBoundViewHolder<V> {
        val binding = createBinding(parent)
        return DataBoundViewHolder(binding)
    }

    protected abstract fun createBinding(parent: ViewGroup): V

    override fun onBindViewHolder(holder: DataBoundViewHolder<V>, position: Int) {
        try {

            val item = this.getItem(position)
            if (item != null) {
                bind(holder.binding, item)
                holder.binding.executePendingBindings()
            }


        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    protected abstract fun bind(binding: V, item: T)
}
