package com.my.assessmentapp.ui.history

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.paging.PagingData
import com.my.assessmentapp.data.local.model.History
import com.my.assessmentapp.data.repository.HistoryRepository
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*

class HistoryViewModel(
    val historyRepository: HistoryRepository,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val shouldRefresh = Channel<Unit>(Channel.CONFLATED)
    private val clearListCh = Channel<Unit>(Channel.CONFLATED)

    val history = flowOf(
        clearListCh.receiveAsFlow().map {
            PagingData.empty<History>()
        },
        shouldRefresh.receiveAsFlow().flatMapLatest {
            var status = "history"
            if (savedStateHandle.getLiveData<String>(KEY_STATUS).value != null) {
                status =
                    savedStateHandle.getLiveData<Int>(KEY_STATUS).value.toString()
            }
            historyRepository.getHistory(status, 6)
        }
    ).flattenMerge(2)

    fun initHistoryListing() {
        savedStateHandle.set(KEY_STATUS, "history")
        clearListCh.offer(Unit)
        shouldRefresh.offer(Unit)
    }

    companion object {
        const val KEY_STATUS = "status"
    }
}