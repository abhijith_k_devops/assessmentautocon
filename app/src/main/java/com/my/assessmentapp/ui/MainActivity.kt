package com.my.assessmentapp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.my.assessmentapp.R
import com.my.assessmentapp.databinding.ActivityMainBinding
import com.my.assessmentapp.ui.common.IOnBackPressed
import com.my.assessmentapp.ui.history.HistoryActivity
import com.my.assessmentapp.ui.printer.PrinterActivity

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.btnHistory.setOnClickListener {
            startActivity(Intent(this@MainActivity,HistoryActivity::class.java))
        }
        binding.btnPrint.setOnClickListener {
            startActivity(Intent(this@MainActivity,PrinterActivity::class.java))
        }
        setContentView(binding.root)
    }

}