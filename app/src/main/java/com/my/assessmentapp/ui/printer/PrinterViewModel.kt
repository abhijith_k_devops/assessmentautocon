package com.my.assessmentapp.ui.printer

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.my.assessmentapp.data.local.model.PrintResponse
import com.my.assessmentapp.utils.FetchDetails

class PrinterViewModel(val context: Context) : ViewModel() {
    val fetchDetails = FetchDetails(context,0)
    private var _printResponse = MutableLiveData<PrintResponse>()
    val printResponse: LiveData<PrintResponse> = _printResponse

    init {
        fetchPrintDetails()
    }

    private fun fetchPrintDetails(){
        _printResponse.value = fetchDetails.getPrintDataFromAsset()
    }
}