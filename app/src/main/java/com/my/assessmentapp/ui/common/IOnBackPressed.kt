package com.my.assessmentapp.ui.common

interface IOnBackPressed {
    fun onBackPressed()
}