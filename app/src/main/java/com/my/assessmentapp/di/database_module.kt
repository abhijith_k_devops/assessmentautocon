package com.my.assessmentapp.di

import com.my.assessmentapp.data.local.db.AssessmentDB
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val databaseModule = module {
    single {
        AssessmentDB.getInstance(androidApplication())
    }
    single { get<AssessmentDB>().historyDao() }
}