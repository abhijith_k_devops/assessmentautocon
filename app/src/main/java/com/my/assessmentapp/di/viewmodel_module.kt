package com.my.assessmentapp.di

import com.my.assessmentapp.ui.history.HistoryViewModel
import com.my.assessmentapp.ui.printer.PrinterViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelModule = module {
    viewModel { HistoryViewModel(savedStateHandle = get(),historyRepository = get()) }
    viewModel { PrinterViewModel(get()) }
}