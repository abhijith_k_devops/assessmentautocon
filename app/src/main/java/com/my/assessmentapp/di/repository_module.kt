package com.my.assessmentapp.di

import com.my.assessmentapp.data.repository.HistoryRepository
import com.my.assessmentapp.data.repository.HistoryRepositoryImpl
import org.koin.dsl.module

val repoModule = module {
    single { HistoryRepositoryImpl(get(), get()) as HistoryRepository }
}