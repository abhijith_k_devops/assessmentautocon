package com.my.assessmentapp.di

import com.my.assessmentapp.di.*

/** Gather all app modules */
val assessmentApp = listOf(
    viewModelModule,
    repoModule,
    databaseModule,
    appExecutorsModule
)
