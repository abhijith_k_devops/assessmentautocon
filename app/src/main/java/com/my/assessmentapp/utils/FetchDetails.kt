package com.my.assessmentapp.utils

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.my.assessmentapp.data.local.model.History
import com.my.assessmentapp.data.local.model.HistoryResponse
import com.my.assessmentapp.data.local.model.PrintResponse
import java.io.IOException

class FetchDetails(val context: Context,val page : Int) {

    fun getJsonDataFromAsset() : HistoryResponse? {
        val jsonString: String
        val historyResponse : HistoryResponse?
        try {
            jsonString = context.assets.open("history$page.json").bufferedReader().use { it.readText() }
            val gson = Gson()
            val historyResponseType = object : TypeToken<HistoryResponse>() {}.type

            historyResponse = gson.fromJson(jsonString, historyResponseType)
            //persons.forEachIndexed { idx, person -> Log.i("data", "> Item $idx:\n$person") }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return historyResponse
    }
    fun getPrintDataFromAsset() : PrintResponse? {
        val jsonString: String
        val printResponse : PrintResponse?
        try {
            jsonString = context.assets.open("print.json").bufferedReader().use { it.readText() }
            val gson = Gson()
            val printResponseType = object : TypeToken<PrintResponse>() {}.type

            printResponse = gson.fromJson(jsonString, printResponseType)
            //persons.forEachIndexed { idx, person -> Log.i("data", "> Item $idx:\n$person") }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return printResponse
    }
}
